function vol = readVolume(volAnnotationDir, volAnnotationFilename, bbox, processData)
% Read the wkw volume defined by the input parameters of this function
% This function optionally smoothes the data if processData == true.

% Author: Ali Karimi <ali.karimi@brain.mpg.de>

% unzip the raw data
if isempty(processData) || ~exist('processData','var')
    processData.flag = false;
end

% Output directory for unzipped wkw files
outdir = fullfile(volAnnotationDir,[volAnnotationFilename,'Raw']);

% create the directory to unzip the files
if ~exist(outdir,'dir')
    util.mkdir(outdir)
    unzip(fullfile(volAnnotationDir,[volAnnotationFilename,'.zip']), outdir);
    unzip(fullfile(outdir,'data'),outdir)
end

% WKW path of first magnification (highest resolution)
source_path = fullfile(outdir,'1');

% This is where the actual reading happens
vol = wkwLoadRoi(source_path, bbox);
assert(any(vol(:)), 'The bouding box is empty');

% Only keep the voxels which have the id of interest. This removes other
% annotations if they exist within the same bounding box
vol = (vol == processData.cellID);

% Flag to process the data by smoothing it with a gaussian kernel (9 x 9 x
% 9) with an STD of 8.
if processData.flag
    assert(~isempty(vol(:)>0));
    % Pad array before smoothing
    vol = padarray(vol,[10,10,10]);
    % Smooth data
    vol = smooth3(vol,'gaussian',9,8);
    % Remove the padding
    vol = vol(11:end-10,11:end-10,11:end-10);
else
    vol = uint32(vol);
    % Note: Give new cell ID in case field newCellID is defined for the processData
    % structure
    if isfield(processData,'newCellID')
        vol(vol>0) = uint32(processData.newCellID);
    else
        vol(vol>0) = uint32(processData.cellID);
    end
end

end










