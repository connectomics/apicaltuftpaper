%% Set-up parameters for reading the volume annotations of the
% V2 dataset as
dataset_name = 'v2';
dataset_Dir = fullfile(util.dir.getSurface, dataset_name);
volAnnotationDir = fullfile(dataset_Dir,...
    'V2_102_l23_ak_straightenedPosthoc_version');
volAnnotationName = 'V2_volumeAnnotations';
% webknossos bounding box for the 4 volume annotations in V2 dataset
bbox = {[3729,1485,2650,1055,1649,272],...
    [5672,1795,3694,358,1711,419],...
    [5117,1384,3968,1518,1715,243],...
    [3236,1529,3904,1594,1756,443]};
% convert from WK style of bbox (x_min,y_min,z_min,x_length,y_length,z_length)
% to matlab style (3x2 array) that contains min and maximum along each
% dimension
bbox_Matlab = cellfun(@Util.convertWebknossosToMatlabBbox,bbox,...
    'UniformOutput',false);

%% Read the volume annotation into an array
% The data would be smoothed if flag == true
processData.flag = true;
vol = cell(1,4);
for i = 1:4
    % cell ID is the integer ID of the pixels in our volume annotation.
    % This matches the index of the bounding box in this case
    processData.cellID = i;
    vol{i} = surface.genIsosurface.readVolume(volAnnotationDir, ...
        volAnnotationName, bbox_Matlab {i}, processData);
end
% Look at one of the volumes
implay (vol{1})

%% Convert to surfaces (collection of triangular surface patches)
isoSurf = cell(1,4);
% Only keep a fraction of the faces
reduceFactor = 0.1;
for i = 1:4
    surfaceThreshhold = 0.2;
     % create the surface at a specific threshhold (isosurface)
    isoSurf{i} = isosurface(vol{i}, surfaceThreshhold);
    % fix order of MATLAB coordinates 
    isoSurf{i}.vertices = reshape(isoSurf{i}.vertices, [], 3);
    isoSurf{i}.vertices = isoSurf{i}.vertices(:, [2, 1, 3]);
    isoSurf{i} = reducepatch(isoSurf{i}, reduceFactor);
end
% remove the vol variable to save on the memory
clear vol;
%% See one of the isosurfaces
figure()
p = patch(isoSurf{1});

% Viewing settings
p.FaceColor = [0.5, 0.5, 0.5];
p.EdgeColor = 'none';
p.FaceAlpha = 0.3;
daspect([1 1 1])
view([0,-90])
axis tight
axis off
camlight
lighting gouraud

%% Write surfaces out for amira (as .ply files)
% Paramater should contain the voxel size as follows:
param.raw.voxelSize = [12, 12, 30];
plyDir = fullfile(dataset_Dir,'plyFiles');
mkdir(plyDir);
% create a cell array with the file names
outputFileNames = arrayfun(@(x) fullfile(plyDir,[num2str(x),'.ply']),...
    1:length(isoSurf),'UniformOutput',false);
% Call to the function
Visualization.exportIsoSurfaceToAmira(param, isoSurf, outputFileNames);