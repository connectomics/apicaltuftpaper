util.clearAll;
% get innervation ratio
apTuft = apicalTuft.getObjects('inhibitoryAxon');
synRatio = apicalTuft.applyMethod2ObjectArray(apTuft,'getSynRatio');

combineRatio = [synRatio{1,5}{1};synRatio{2,5}{1}];
totalApicalSpec = combineRatio.L2Apical + combineRatio.DeepApical;
disp(mean(totalApicalSpec))
disp(util.stat.sem(totalApicalSpec,[],1))