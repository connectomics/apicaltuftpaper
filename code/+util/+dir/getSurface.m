function [annotationDir] = getSurface()
% Author: Ali Karimi <ali.karimi@brain.mpg.de>

annotationDir = fullfile(util.dir.getMain,'data',...
    'Other','isoSurfaceGeneration');
end

